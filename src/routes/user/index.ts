import { Express, Router, Request, Response, NextFunction } from 'express';

import { User } from '../../db/mongo/models/User';

import log from '../../helpers/logger';

export const userRouter: Router = Router();


// get all
userRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
    log.get('/api/auth');
    User.find({}, (err, users) => {
        if (err) {
            log.error(JSON.stringify(err));
        } else {
            res.json({
                success: true,
                msg: 'get all users',
                data: users
            })
        }
    })
});

// get by id
userRouter.get('/id/:id', (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id;
    log.get('/api/auth/id ==> ' + id);
    User.find({'_id': id }, (err, user) => {
        if (err) {
            log.error(JSON.stringify(err));
        } else {
            res.json({
                success: true,
                msg: 'get user by id',
                data: user
            });
        }
    });
});
// get by name
userRouter.get('/name/:username', (req: Request, res: Response, next: NextFunction) => {
    const username = req.params.username;
    log.get('/api/auth/name ==> ' + username);
    User.find({'username': username}, (err, user) => {
        if (err) {
            log.error(JSON.stringify(err));
        } else {
            res.json({
                success: true,
                msg: 'get user by username',
                data: user
            });
        }
    });
});

// update
userRouter.put('/', (req: Request, res: Response, next: NextFunction) => {
    log.put('/api/auth => ' + JSON.stringify(req.body));
    const request = req.body;
    const newUser = {
        name: request.name,
        users: request.users
    };
    User.findOneAndUpdate(request._id, newUser, (err) => {
        if (err) {
            log.error(JSON.stringify(err));
        } else {
            res.json({
                success: true,
                msg: 'user updated',
            });
        }
    });
});
// delete
userRouter.delete('/:id', (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id;
    log.delete('/auth/:id ==> ' + id);
});
