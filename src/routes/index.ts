
import { Express, Router, Request, Response, NextFunction } from 'express';

export const indexRouter: Router = Router();
/* GET home page. */
indexRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
  res.json({
    success: true,
    msg: 'get index'
  });
});

