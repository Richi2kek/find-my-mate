import { Express, Router, Request, Response, NextFunction } from 'express';

import { User } from '../../db/mongo/models/User';

import log from '../../helpers/logger';


export const authRouter: Router = Router();

authRouter.post('/register', (req: Request, res: Response, next: NextFunction) => {
    log.post('/api/auth/register => ' + JSON.stringify(req.body));
    const request = req.body;
    console.log(request);
    const user = new User({
        username: request.username,
        password: request.password
    });
    user.save((err) => {
        if(err) {
            console.error(err);
            res.json({
                success: false,
                msg: 'user creation error'
            });
        } else {
            res.json({
                success: true,
                msg: 'user created'
            });
        }
    })
});

authRouter.post('/login', (req: Request, res: Response, next: NextFunction) => {
    log.post('/api/auth/login => ' + JSON.stringify(req.body));
    const request = req.body;
});
