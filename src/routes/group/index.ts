import { Express, Router, Request, Response, NextFunction } from 'express';

import { Group } from '../../db/mongo/models/Group';

import log from '../../helpers/logger';

export const groupRouter: Router = Router();
/* GET home page. */
groupRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
  log.get('/api/group');

  Group.find({}, (err, groups) => {
    if (err) {
      log.error(JSON.stringify(err));
    } else {
      res.json({
        success: true,
        msg: 'get all group',
        data: groups
      });
    }
  });
});

groupRouter.post('/', (req: Request, res: Response, next: NextFunction) => {
  log.post('/api/group => ' + JSON.stringify(req.body));
  const request = req.body;
  const group = new Group({
    name: request.name,
    users: request.users
  });
  group.save((err) => {
    if (err) {
      log.error(JSON.stringify(err));
    } else {
      res.json({
        success: true,
        msg: 'group posted'
      });
    }
  });
});

groupRouter.put('/', (req: Request, res: Response, next: NextFunction) => {
  log.put('/api/group => ' + JSON.stringify(req.body));
  const request = req.body;
  const newGroup = {
    name: request.name,
    users: request.users
  };
  Group.findOneAndUpdate(request._id, newGroup, (err) => {
    if (err) {
      log.error(JSON.stringify(err));
    } else {
      res.json({
        success: true,
        msg: 'group updated',
      });
    }
  });
});