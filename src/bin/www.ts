
import app from '../app';

import log from '../helpers/logger';

const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

app.listen(port, onListening);

function normalizePort(val: any) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

function onListening() {
  log.info("server listening on http://localhost:" + port);
}