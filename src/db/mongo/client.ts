import * as mongoose from 'mongoose';

import { dbConfig } from '../../config/db';
import log from '../../helpers/logger';

export class MongoClient {
    public db: mongoose.Connection;
    private url: string;
    private port: string;
    private database: string;
    constructor() {
        this.url = dbConfig.mongo.url;
        this.port = dbConfig.mongo.port;
        this.database = dbConfig.mongo.database;
        mongoose.connect(`mongodb://${this.url}:${this.port}/${this.database}`, {
            useMongoClient: true
        });

        (<any>mongoose).Promise = global.Promise; // little hack to set mongoose Promise with new es6 Promise
        this.db = mongoose.connection;
        this.db.on('error', () => {
            log.error('Mongodb connection error!');
        });
        this.db.on('connected', () => {
            log.info('Mongodb successfully connected!')
        });
        process.on('SIGINT', () => {
            this.db.close(() => {
                log.info('Mongoose default connection disconnected!');
                process.exit(0);
            });
        })
    }

}

