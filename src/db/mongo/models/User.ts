import * as mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
    username: String,
    password: String,
    groupId: Number
});

export const User = mongoose.model('UserModel', UserSchema);

