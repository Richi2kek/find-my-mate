import * as mongoose from 'mongoose';

const GroupSchema = new mongoose.Schema({
    name: String,
    users: [String]
});

export const Group = mongoose.model('GroupModel', GroupSchema);