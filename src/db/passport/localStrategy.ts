import { Strategy as LocalStrategy } from 'passport-local';
import * as mongoose from 'mongoose';
import { User } from '../mongo/models/User';

import log from '../../helpers/logger';

import { serverConfig } from '../../config/server';
// jwt strat
export class HookLocalStrategy {
    private passport: any;
    private options: any = {};
    private model: mongoose.Model<any>;

    constructor(passport: any) {
        this.passport = passport;
        this.model = User;
        passport.use(new LocalStrategy(
            (username: string, password: string, callback: any) => {
                this.model.find({'username': username}, (err, results) => {
                    if (err) {
                        log.error(JSON.stringify(err));
                    } else {
                        if (results.length) {
                            let user = results[0];
                            log.info(JSON.stringify(user));
                        }
                    }
                });
            }));
    }
}