export const dbConfig = {
    mongo: {
        url: 'localhost',
        database: 'find-my-mate',
        port: '27017'
    },
    hash: {
        saltRound: 10
    }
};

